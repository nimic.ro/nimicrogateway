import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NimicgatewayTestModule } from '../../../../test.module';
import { ConferenceDetailComponent } from 'app/entities/nimicconferences/conference/conference-detail.component';
import { Conference } from 'app/shared/model/nimicconferences/conference.model';

describe('Component Tests', () => {
  describe('Conference Management Detail Component', () => {
    let comp: ConferenceDetailComponent;
    let fixture: ComponentFixture<ConferenceDetailComponent>;
    const route = ({ data: of({ conference: new Conference('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [NimicgatewayTestModule],
        declarations: [ConferenceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ConferenceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ConferenceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load conference on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.conference).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
