import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ConferenceComponentsPage, ConferenceDeleteDialog, ConferenceUpdatePage } from './conference.page-object';

const expect = chai.expect;

describe('Conference e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let conferenceComponentsPage: ConferenceComponentsPage;
  let conferenceUpdatePage: ConferenceUpdatePage;
  let conferenceDeleteDialog: ConferenceDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Conferences', async () => {
    await navBarPage.goToEntity('conference');
    conferenceComponentsPage = new ConferenceComponentsPage();
    await browser.wait(ec.visibilityOf(conferenceComponentsPage.title), 5000);
    expect(await conferenceComponentsPage.getTitle()).to.eq('nimicgatewayApp.nimicconferencesConference.home.title');
  });

  it('should load create Conference page', async () => {
    await conferenceComponentsPage.clickOnCreateButton();
    conferenceUpdatePage = new ConferenceUpdatePage();
    expect(await conferenceUpdatePage.getPageTitle()).to.eq('nimicgatewayApp.nimicconferencesConference.home.createOrEditLabel');
    await conferenceUpdatePage.cancel();
  });

  it('should create and save Conferences', async () => {
    const nbButtonsBeforeCreate = await conferenceComponentsPage.countDeleteButtons();

    await conferenceComponentsPage.clickOnCreateButton();
    await promise.all([
      conferenceUpdatePage.setNameInput('name'),
      conferenceUpdatePage.setParticipantsInput('5'),
      conferenceUpdatePage.setDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM')
    ]);
    expect(await conferenceUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await conferenceUpdatePage.getParticipantsInput()).to.eq('5', 'Expected participants value to be equals to 5');
    expect(await conferenceUpdatePage.getDateInput()).to.contain('2001-01-01T02:30', 'Expected date value to be equals to 2000-12-31');
    await conferenceUpdatePage.save();
    expect(await conferenceUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await conferenceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Conference', async () => {
    const nbButtonsBeforeDelete = await conferenceComponentsPage.countDeleteButtons();
    await conferenceComponentsPage.clickOnLastDeleteButton();

    conferenceDeleteDialog = new ConferenceDeleteDialog();
    expect(await conferenceDeleteDialog.getDialogTitle()).to.eq('nimicgatewayApp.nimicconferencesConference.delete.question');
    await conferenceDeleteDialog.clickOnConfirmButton();

    expect(await conferenceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
