/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nimicro.gateway.web.rest.vm;
