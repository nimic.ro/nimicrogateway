import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'conference',
        loadChildren: () => import('./nimicconferences/conference/conference.module').then(m => m.NimicconferencesConferenceModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class NimicgatewayEntityModule {}
