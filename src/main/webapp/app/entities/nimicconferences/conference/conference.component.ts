import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IConference } from 'app/shared/model/nimicconferences/conference.model';
import { ConferenceService } from './conference.service';
import { ConferenceDeleteDialogComponent } from './conference-delete-dialog.component';

@Component({
  selector: 'jhi-conference',
  templateUrl: './conference.component.html'
})
export class ConferenceComponent implements OnInit, OnDestroy {
  conferences?: IConference[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected conferenceService: ConferenceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.conferenceService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<IConference[]>) => (this.conferences = res.body ? res.body : []));
      return;
    }
    this.conferenceService.query().subscribe((res: HttpResponse<IConference[]>) => {
      this.conferences = res.body ? res.body : [];
      this.currentSearch = '';
    });
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInConferences();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IConference): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInConferences(): void {
    this.eventSubscriber = this.eventManager.subscribe('conferenceListModification', () => this.loadAll());
  }

  delete(conference: IConference): void {
    const modalRef = this.modalService.open(ConferenceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.conference = conference;
  }
}
