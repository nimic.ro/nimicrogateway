import { Moment } from 'moment';

export interface IConference {
  id?: string;
  name?: string;
  participants?: number;
  date?: Moment;
}

export class Conference implements IConference {
  constructor(public id?: string, public name?: string, public participants?: number, public date?: Moment) {}
}
